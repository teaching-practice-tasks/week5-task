### Week 5: Find anagrams (but with dictionaries)

Implement a Python function
`find_anagrams(words: List[str]) -> Dict[str, List[str]]` that takes in
a list of words, `words`, and returns a dictionary where the keys are the sorted characters of each word and the values
are lists of words that are anagrams of each other. The words in the list of anagrams should be in the same order as
they appear in the original list.

Example:

```
words = ["cat", "dog", "act", "god", "tac"]

assert find_anagrams(words) == {
    'act': ['cat', 'act', 'tac'],
    'dgo': ['dog', 'god'],
}
```

Note:

* The function should work for lists of any size.
* The function should not use any external libraries.
* The function should be case-insensitive.

You have to implement function `find_anagrams(words: List[str]) -> Dict[str, List[str]]` in `solution.py` file.
You may create additional functions.
