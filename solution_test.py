import unittest

from solution import find_anagrams


class TestSolution(unittest.TestCase):
    def test_1(self):
        words = ["cat", "dog", "rat", "bat"]
        expected_output = {
            'act': ['cat'],
            'dgo': ['dog'],
            'abt': ['bat'],
            'art': ['rat'],
        }
        assert find_anagrams(words) == expected_output

    def test_2(self):
        words = ["cat", "dog", "act", "god", "tac"]
        expected_output = {
            'act': ['cat', 'act', 'tac'],
            'dgo': ['dog', 'god'],
        }
        assert find_anagrams(words) == expected_output

    def test_3(self):
        words = ["cat", "dog", "act", "god", "tac", "god", "act", "dog", "act"]
        expected_output = {
            'act': ['cat', 'act', 'tac', 'act', 'act'],
            'dgo': ['dog', 'god', 'god', 'dog'],
        }
        assert find_anagrams(words) == expected_output

    def test_4(self):
        words = ["cat", "Dog", "act", "gOd", "tac"]
        expected_output = {
            'act': ['cat', 'act', 'tac'],
            'dgo': ['Dog', 'gOd'],
        }
        assert find_anagrams(words) == expected_output

    def test_5(self):
        words = []
        expected_output = {}
        assert find_anagrams(words) == expected_output
